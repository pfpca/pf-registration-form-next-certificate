<?php

function getSelf() {
  $self = $_SERVER['PHP_SELF'];
  if (substr($self, -9) === "index.php") {
    $self = substr($self, 0, -9);
  }
  return $self;
}

// Make sure SSL is used
if ($_SERVER['HTTPS'] != "on") {
  $url = $_SERVER['SERVER_NAME'];
  $path = getSelf();
  header("Location: https://$url$path");
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Course Fee Payment | Pittsburgh Arts</title>
  <link rel="stylesheet" href="stylesheet.css" type="text/css" />
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24217007-1']);
  _gaq.push(['_setDomainName', '.pittsburgharts.org']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<script type="text/javascript">

function calculate(){
//Assess late fee BEGIN
  var today = new Date();
  var lateDate = new Date();
  var lateFee;
  lateDate.setHours(0,0,0);
  //Months are indexed at 0
  //set late fee date to one day past
  if(document.formname.semester.value=="Spring 2018"){
    lateDate.setFullYear(2018, 0, 5);
    }
  else if(document.formname.semester.value=="Winter-Spring 2016"){
    lateDate.setFullYear(2018, 4, 8);
    }
  else{
    lateFee="true";
    }

  if (today<lateDate){
    lateFee="false";
    }
  else{
    lateFee="true";
    }
//Assess late fee END


var fee1=parseInt(document.formname.fee1.value);
var fee2=parseInt(document.formname.fee2.value);
var fee3=parseInt(document.formname.fee3.value);
var lateFeePrice;
if(lateFee=="true"){
  lateFeePrice=0;
  if(fee1!=0 || fee2!=0 || fee3!=0){
    lateFeePrice+=15;
  }
document.getElementById("lateFee").style.display="block";
}
else{
lateFeePrice=0;
document.getElementById("lateFee").style.display="none";
}
document.formname.lateFee.value=lateFee;
document.formname.paymentTotal.value=fee1+fee2+fee3+lateFeePrice;
}


//END of SCRIPT
</script>

</head>
<?php

$privateKey = '6Lej5AAAAAAAAN3Mkgwj7VWIsC_M14QhQbMKpo86';
$publicKey = '6Lej5AAAAAAAAIW_kou1ksjrDn97dCQl9LLRq5lu';

//session_start();
global $PHP_SELF;

$failed = 0;          // set to 1 (true) on any failure
$failed_submit = 0;     // set to 1 (true) if form has not been submitted

$failed_fname = 0;        // set to 1 (true) if first name is invalid
$failed_lname = 0;        // set to 1 (true) if last name is invalid
$failed_address = 0;      // set to 1 (true) if street address is invalid
$failed_city = 0;         // set to 1 (true) if city is invalid
$failed_state = 0;        // set to 1 (true) if state is invalid
$failed_zip = 0;          // set to 1 (true) if zip code is invalid
$failed_dphone = 0;       // set to 1 (true) if day phone is invalid
$failed_nphone = 0;       // set to 1 (true) if night phone is invalid
$failed_email = 0;        // set to 1 (true) if email is invalid
$failed_course = 0;             // set to 1 (true) if first course selection not completed
$failed_school = 0;       // set to 1 (true) if school not selected
$failed_ged = 0;        // set to 1 (true) if date of high school graduation is invalid
$failed_birth = 0;        // set to 1 (true) if date of birth is invalid
$failed_refund = 0;       // set to 1 (true) if refund checkbox is invalid
$failed_total = 0;        // set to 1 (true) if total amount is invalid
$failed_ccType = 0;       // set to 1 (true) if credit card type is invalid
$failed_ccExp = 0;        // set to 1 (true) if credit card expiration is invalid
$failed_ccName = 0;       // set to 1 (true) if credit card print name is invalid
$failed_ccNumber = 0;     // set to 1 (true) if credit card number is invalid
$failed_denied = 0;       // set to 1 (true) if credit card is denied


// if form has not been submitted
if(!isset($_POST["submit"])) { $failed = 1; $failed_submit = 1; }

if (!$failed_submit) // only set other failed values if the form has been submitted
{




  //==============================================================================
  //
  //  VALIDATION: Check the POST variables and track any invalid required fields
  //
  //==============================================================================

  if (trim($_POST["firstname"]) == "") { $failed = 1; $failed_fname = 1; }
  if (trim($_POST["lastname"]) == "") { $failed = 1; $failed_lname = 1; }
  if (trim($_POST["address"]) == "") { $failed = 1; $failed_address = 1; }
  if (trim($_POST["city"]) == "") { $failed = 1; $failed_city = 1; }
  if (trim($_POST["state"]) == "" || trim($_POST["state"]) == "9999") { $failed = 1; $failed_state = 1; }
  if (trim($_POST["zip"]) == "") { $failed = 1; $failed_zip = 1; }
  if (trim($_POST["dphone"]) == "") { $failed = 1; $failed_dphone = 1; }

  if (trim($_POST["email"]) == "") { $failed = 1; $failed_email = 1; }
  $empattern = "^[A-Za-z0-9_%][A-Za-z0-9._%+-]+@[A-Za-z0-9_%][A-Za-z0-9._%-]+\.[A-Za-z]{2,4}$";
  $theresult = ereg($empattern, $_POST["email"]);
  if ($theresult) {} else { $failed = 1; $failed_email = 1; }
  if (trim($_POST["school"]) == "null") { $failed = 1; $failed_school = 1; }
  if (trim($_POST["semester"]) == "unselected") { $failed = 1; $failed_semester = 1; }

  if (trim($_POST["paymentTotal"]) == "") { $failed = 1; $failed_total = 1; }
  if (trim($_POST["ccType"]) == "") { $failed = 1; $failed_ccType = 1; }
  if ($_POST["ccExp"] == "") { $failed = 1; $failed_ccExp = 1; }
  if (trim($_POST["ccName"]) == "") { $failed = 1; $failed_ccName = 1; }

  if (trim($_POST["ccNumber"]) == "") { $failed = 1; $failed_ccNumber = 1; }
  $CCpattern = "^([0-9]{16}|[0-9]{4}[ -][0-9]{4}[ -][0-9]{4}[ -][0-9]{4}|[0-9]{15}|[0-9]{14}|[0-9]{13})$";
  $result = ereg($CCpattern, $_POST["ccNumber"]);
  if ($result) {} else { $failed = 1; $failed_ccNumber = 1; }

}

if (isset($_POST["filmmaker"]) && "process" == $_POST["filmmaker"] && $failed != 1) {

  //=============================================================================
  //
  //  VALID FORM: If the form has passed validation, collect the POST variables
  //
  //=============================================================================

  $fname = $_POST["firstname"];
  $lname = $_POST["lastname"];
  $add = $_POST["address"];
  $city = $_POST["city"];
  $state = $_POST["state"];
  $zip = $_POST["zip"];
  $dphone = $_POST["dphone"];
  $email = $_POST["email"];

  $school=$_POST["school"];
  $semester=$_POST["semester"];
  $fee1 = $_POST["fee1"];
  $fee2 = $_POST["fee2"];
  $fee3 = $_POST["fee3"];
  $lateFee = $_POST["lateFee"];


  $check = $_POST["paymentType[0]"];
  $cash = $_POST["paymentType[1]"];
  $cc = $_POST["paymentType[2]"];
  $payT = $_POST["paymentTotal"];
  $donateT = $_POST["donateTotal"];

  $ccType = $_POST["ccType"];
  $ccExp = $_POST["ccExp"];
  $ccName = $_POST["ccName"];
  $ccNumber = $_POST["ccNumber"];


  //======================================================================
  //
  //  AUTHORIZE.net: Submit the credit card transaction to Authorize.net
  //
  //======================================================================

  $auth_net_login_id      = "7q64mvSPcj77";
  $auth_net_tran_key      = "7S67g4666JmmX7v7";
  $auth_net_url       = "https://secure.authorize.net/gateway/transact.dll";

  $authnet_values       = array
  (
    "x_login"       => $auth_net_login_id,
    "x_delim_char"      => "|",
    "x_delim_data"      => "TRUE",
    "x_url"         => "FALSE",
    "x_type"        => "AUTH_CAPTURE",
    "x_method"        => "CC",
    "x_tran_key"      => $auth_net_tran_key,
    "x_relay_response"    => "FALSE",
    "x_card_num"      => $ccNumber,
    "x_exp_date"      => $ccExp,
    "x_description"     => "Course Fees",
    "x_amount"        => $payT,
    "x_first_name"      => $fname,
    "x_last_name"     => $lname,
    "x_address"       => $add,
    "x_city"        => $city,
    "x_state"       => $state,
    "x_zip"         => $zip,
  );
  $fields = "";

  foreach( $authnet_values as $key => $value ) $fields .= "$key=" . urlencode( $value ) . "&";

  $ch = curl_init("https://secure.authorize.net/gateway/transact.dll");
  curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
  curl_setopt($ch, CURLOPT_POSTFIELDS, rtrim( $fields, "& " )); // use HTTP POST to send form data
  $resp = curl_exec($ch); //execute post and get results
  curl_close ($ch);

  $text = $resp;

  $auth_response = explode("|",$text);
  $approved_denied = str_replace("\"","",$auth_response[0]);

  switch($approved_denied) {
  case 1:
    $fval = "Approved";
    $failed_denied = 0;
    break;
  default:
    $fval = "Declined";
    $failed_denied = 1;
    $failed = 1;
    break;
  }


  //====================================================================
  //
  //  EMAIL: Send registration email to staff at Pittsburgh Filmmakers
  //
  //====================================================================


  $staffEmail = 'onlinereg@pghfilmmakers.org';
  // send email to testers
  //$testEmail1 = 'webteam@pittsburgharts.org';

  if($lateFee=="true"){$lateFeeText="+$15 Late Fee";}

  /* SEMESTER CHANGE */
  $body = 'External Student Course Fee Payment.
      First Name: '.$fname.'
      Last Name: '.$lname.'
      Email: '.$email.'
      Address: '.$add.'
      City: '.$city.'
      State: '.$state.'
      Zip: '.$zip.'
      Day Phone: '.$dphone.'

      School: '.$school.'
      Semester: '.$semester.'
      Fee1: '.$fee1.'
      Fee2: '.$fee2.'
      Fee3: '.$fee3.'

      '.$lateFeeText.'

      Total Amount Charged: $'.$payT.'
      Payment Type: '.$ccType.'
      Print Name: '.$ccName.'
      Card Expire:'.$ccExp;


  $fromAdd = "online@pghfilmmakers.org";

  $subject = 'Pittsburgh Filmmaker\'s '.$semester.' Course Fee Payment Receipt';

  ini_set("sendmail_from","online@pghfilmmakers.org");

  mail($staffEmail, $subject, $body, "From: $fromAdd");
  mail($testEmail1, $subject, $body, "From: $fromAdd");
  mail($testEmail2, $subject, $body, "From: $fromAdd");
  mail($email, $subject, $body, "From: $fromAdd");


  //===========================================================================================
  //
  //  RESPONSE: Thank the user for registering and display the information that was submitted
  //
  //===========================================================================================

  if ($failed_denied == 0) { //Thank you page for registration
  ?>
  <html>
  <head>
  <title>Thank You!</title>
  </head>
  <body>
  <h1>Thank You!</h1>
  <h2>Your course fee of <?php print"$payT";?> has been payed.</h2>
  <h3>A receipt of the charges has been sent to <?php print"$email";?> for your records</h3>
  <h3>If there was any issue with your payment processing, you can contact Dory, the Pittsburgh Filmmakers registrar, at 412.681.5449</h3>
  </body>
  </html>
  <?
  }

}


//===================================================================================================
//
//  FORM: Display the form if it hasn't yet been submitted or if the previous submission has failed
//
//===================================================================================================

if (!isset($_POST["filmmaker"]) || $failed != 0 || $failed_denied != 0) {


?>


<div class="page">

<div class="text">

<h1>Pittsburgh Filmmakers Course Fee Payment</h1>
<strong>If you have questions or technical problems, you can call the registrar at 412-681-5449.</strong>

</div>
<?

  //===================================================================================================
  //
  //  VALIDATION WARNINGS: If the form submission has failed, display any validation warning messages
  //
  //===================================================================================================

  if ($failed && !$failed_submit) { ?> <br /><span class="message">Please correct the following:</span><br /> <?
    if ($failed_fname) { ?> <span class="failure">You didn't enter a First Name.</span><br /> <? }
    if ($failed_lname) { ?> <span class="failure">You didn't enter a Last Name.</span><br /> <? }
    if ($failed_address) { ?> <span class="failure">You didn't enter a Street Address.</span><br /> <? }
    if ($failed_city) { ?> <span class="failure">You didn't enter a City.</span><br /> <? }
    if ($failed_state) { ?> <span class="failure">The didn't enter a State.</span><br /> <? }
    if ($failed_zip) { ?> <span class="failure">You didn't enter a Zip Code.</span><br /> <? }
    if ($failed_dphone) { ?> <span class="failure">You didn't enter a Day Phone.</span><br /> <? }

    if ($failed_email) { ?> <span class="failure">The Email address is invalid.</span><br /> <? }
    if ($failed_school) { ?> <span class="failure">You didn't select your School.</span><br /> <? }
    if ($failed_semester) { ?> <span class="failure">You didn't select a Semester.</span><br /> <? }

    if ($failed_total) { ?> <span class="failure">You didn't enter a Total Amount.</span><br /> <? }
    if ($failed_ccType) { ?> <span class="failure">You didn't select a credit card type.</span><br /> <? }
    if ($failed_ccExp) { ?> <span class="failure">The expiration date of your credit card is invalid.</span><br /> <? }
    if ($failed_ccName) { ?> <span class="failure">The name on your credit card is invalid.</span><br /> <? }
    if ($failed_ccNumber) { ?> <span class="failure">The credit card number you entered is invalid.</span><br /> <? }
    if ($failed_denied) { ?> <span class="failure">Your credit card transaction was declined. Please make sure you entered the correct information.</span><br /> <? }
  }

?>


  <form name="formname" method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">


    <fieldset><legend>Student Contact</legend>
      <label class="input-label" for="fn">First Name: </label><input name="firstname" type="text" id="fn" value="<? echo $_POST['firstname'] ?>" />
      <br/>
      <label class="input-label" for="ln">Last Name: </label><input name="lastname" type="text" id="ln" value="<? echo $_POST['lastname'] ?>" />
      <br />

      <label class="input-label" for="dp">Phone #:</label><input name="dphone" type="text" id="dp" value="<?php echo $_POST['dphone'] ?>" />
      <br/>
      <label class="input-label" for="em">Email: </label><input name="email" type="text" id="em" value="<?php echo $_POST['email'] ?>" />
      <br/>
    </fieldset>


    <fieldset><legend>Fee Information</legend>

      <label class="input-label" for="school">College or University:</label>
      <select name="school" id="school" required>
      <option value="null">Select Your School</option>
      <option value="PITT">Pitt</option>
      <option value="DUQ">Duq</option>
      <option value="RMU">RMU</option>
      <option value="CAR">Carlow</option>
      <option value="CMU">CMU</option>
      <option value="SHU">SHU</option>
      <option value="CHAT">Chatham</option>
      </select>
      <br/>
      <label class="input-label" for="semester">Semester:</label>
      <select name="semester" id="semester" onchange="calculate()" required>
        <option value="unselected">Select Your Semester</option>
	<option value="Fall 2015">Fall 2015</option>
        <option value="Winter-Spring 2016">Winter-Spring 2016</option>
      </select>
      <br/>
      <label for="fee1" class="input-label">Course 1:</label>
      <select name="fee1" id="fee1" onchange="calculate()" required>
      <option value="0">Select Fee</option>
      <option value="35">35</option>
      <option value="70">70</option>
      </select>
      <br/>
      <label for="fee2" class="input-label">Course 2:</label>
      <select name="fee2" id="fee2" onchange="calculate()">
      <option value="0">Select Fee</option>
      <option value="35">35</option>
      <option value="70">70</option>
      </select>
      <br/>
      <label for="fee3" class="input-label">Course 3:</label>
      <select name="fee3" id="fee3" onchange="calculate()">
      <option value="0">Select Fee</option>
      <option value="35">35</option>
      <option value="70">70</option>
      </select>
      <br/>
      <br/>
      <input type="hidden" name="lateFee"></input>
    </fieldset>


    <fieldset style="position:relative"><legend>Payment</legend>
          <label class="input-label" for="ccnm">Name on Card: </label><input name="ccName" type="text" id="ccnm" value="<?php echo $_POST['ccName']; ?>" />
          <br />
          <label class="input-label" for="ad">Street Address:</label><input name="address" type="text" id="ad" value="<? echo $_POST['address'] ?>" />
      <br />
      <label class="input-label" for="city">City:</label><input name="city" type="text" id="city" value="<? echo $_POST['city'] ?>" />
      <br/>
      <label class="input-label" for="st">State:</label>

      <?php

        $states = array ("AL" => "Alabama", "AK" => "Alaska", "AZ" => "Arizona",
          "AR" => "Arkansas", "CA" => "California", "CO" => "Colorado",
          "CT" => "Connecticut", "DE" => "Delaware",
          "DC" => "District of Columbia", "FL" => "Florida",
          "GA" => "Georgia", "HI" => "Hawaii", "ID" => "Idaho",
          "IL" => "Illinois", "IN" => "Indiana", "IA" => "Iowa",
          "KS" => "Kansas", "KY" => "Kentucky", "LA" => "Louisiana",
          "ME" => "Maine", "MD" => "Maryland", "MA" => "Massachusetts",
          "MI" => "Michigan", "MN" => "Minnesota", "MS" => "Mississippi",
          "MO" => "Missouri", "MT" => "Montana", "NE" => "Nebraska",
          "NV" => "Nevada", "NH" => "New Hampshire", "NJ" => "New Jersey",
          "NM" => "New Mexico", "NY" => "New York",
          "NC" => "North Carolina", "ND" => "North Dakota",
          "OH" => "Ohio", "OK" => "Oklahoma", "OR" => "Oregon",
          "PA" => "Pennsylvania", "RI" => "Rhode Island",
          "SC" => "South Carolina", "SD" => "South Dakota",
          "TN" => "Tennessee", "TX" => "Texas", "UT" => "Utah",
          "VT" => "Vermont", "VA" => "Virginia", "WA" => "Washington",
          "WV" => "West Virginia", "WI" => "Wisconsin",
          "WY" => "Wyoming");

          $menuname = "state";

          echo "<SELECT name=\"$menuname\" id=\"st\">";
          echo "<OPTION value=\"9999\" >Please select...</OPTION>";
          while (list($value, $statename) = each ($states)) {
              $opt_string = "<OPTION value=".$value;
              if ($_POST['state'] == $value) { $opt_string .= " selected"; }
              $opt_string .= ">".$statename."</OPTION>";
            echo $opt_string;
          }
          echo "</SELECT>";
      ?>

      <br/>

      <label class="input-label" for="zp">Zip Code:</label><input name="zip" type="text" id="zp" value="<?php echo $_POST['zip'] ?>" />

      <br/>
      <br/>

      <span class="input-label">Card Type:</span>
      <select name="ccType">
        <option value="visa" <?php if ($_POST['ccType'] == "visa") { echo 'selected'; } ?> >Visa</option>
        <option value="mc" <?php if ($_POST['ccType'] == "mc") { echo 'selected'; } ?> >MasterCard</option>
        <option value="discov" <?php if ($_POST['ccType'] == "discov") { echo 'selected'; } ?> >Discover</option>
        <option value="amex" <?php if ($_POST['ccType'] == "amex") { echo 'selected'; } ?> >American Express</option>
     </select>

      <br/>

      <label class="input-label" for="cnum">Card Number: </label><input name="ccNumber" type="text" id="cnum" value="<?php echo $_POST['ccNumber'];?>"/>

      <br/>

      <label class="input-label" for="ccex">Exp Date: </label><input name="ccExp" type="text" id="ccex" value="<?php echo $_POST['ccExp']; ?>" />

      <!-- (c) 2005, 2012. Authorize.Net is a registered trademark of CyberSource Corporation -->
      <div class="AuthorizeNetSeal" style="position:absolute; top:10px; right:10px;">
        <script type="text/javascript" >var ANS_customer_id="83fcfa08-b9ed-4338-be8b-fb32ba04f4a7";</script> <script type="text/javascript"  src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Electronic Check Processing</a>
      </div>

    </fieldset>


    <fieldset><legend>Payment Section</legend><!-- Credit Card Payment Box Start-->

        <label class="req" for="pt">Total Amount: $ </label><input name="paymentTotal" type="text" id="pt" value="<?php echo $_POST['paymentTotal']; ?>" readonly /><span id="lateFee" style="display:none;">$15 Late Fee Added</span>

        <input name="filmmaker" type="hidden" value="process" />
        <br/>
        <br/>
        <input name="submit" type="submit" tabindex="65" value="Submit" />

    </fieldset>


  </form>

</div><!-- End Div Class Page-->

<?

}

?>
