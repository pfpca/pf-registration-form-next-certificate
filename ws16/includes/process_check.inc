<?php
if(!isset($paymentProcessed) || !$paymentProcessed){
  $auth_net_login_id      = "7q64mvSPcj77";
  $auth_net_tran_key      = "7S67g4666JmmX7v7";
  $auth_net_url       = "https://secure.authorize.net/gateway/transact.dll";

  $authnet_values       = array
  (
    "x_login"       => $auth_net_login_id,
    "x_delim_char"      => "|",
    "x_delim_data"      => "TRUE",
    "x_url"         => "FALSE",
    "x_type"        => "AUTH_CAPTURE",
    "x_method"        => "CC",
    "x_tran_key"      => $auth_net_tran_key,
    "x_relay_response"    => "FALSE",
    "x_card_num"      => $reg_vars['cardnumber'],
    "x_exp_date"      => $reg_vars['cardexpiration'],
    "x_description"     => "Course Fees",
    "x_amount"        => $reg_vars['cost'],
    "x_first_name"      => $reg_vars['paymentfirstname'],
    "x_last_name"     => $reg_vars['paymentlastname'],
    "x_address"       => $reg_vars['paymentaddress'],
    "x_city"        => $reg_vars['paymentcity'],
    "x_state"       => $reg_vars['paymentstate'],
    "x_zip"         => $reg_vars['paymentzip'],
  );
  $fields = "";

  foreach( $authnet_values as $key => $value ) $fields .= "$key=" . urlencode( $value ) . "&";

  $ch = curl_init("https://secure.authorize.net/gateway/transact.dll");
  curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
  curl_setopt($ch, CURLOPT_POSTFIELDS, rtrim( $fields, "& " )); // use HTTP POST to send form data
  $resp = curl_exec($ch); //execute post and get results
  curl_close ($ch);

  $text = $resp;

  $auth_response = explode("|",$text);
  $approved_denied = str_replace("\"","",$auth_response[0]);

  switch($approved_denied) {
  case 1:
    $fval = "Approved";
    $paymentProcessed=true;
    break;
  default:
    $fval = "Declined";
    $paymentProcessed=false;
    set_form_error('Payment: Your credit card transaction was declined. Please make sure you entered the correct information.');
    break;
  }

}

?>
