<?php

$reg_vars=array();

$reg_vars['credit']=$_POST['credit'];
$reg_vars['title']=$_POST['title'];
$reg_vars['section']=$_POST['section'];

$reg_vars['prereq']=$_POST['prereq'];

//profile.php
$reg_vars['contactfirstname']=$_POST['contactfirstname'];
$reg_vars['contactlastname']=$_POST['contactlastname'];
$reg_vars['contactphone']=$_POST['contactphone'];
$reg_vars['contactemail']=$_POST['contactemail'];
$reg_vars['contactaddress']=$_POST['contactaddress'];
$reg_vars['contactcity']=$_POST['contactcity'];
$reg_vars['contactstate']=$_POST['contactstate'];
$reg_vars['contactzip']=$_POST['contactzip'];

$reg_vars['hsgradyear']=$_POST['hsgradyear'];
$reg_vars['dateofbirth']=$_POST['dateofbirth'];
$reg_vars['profilemembership']=$_POST['profilemembership'];
$reg_vars['membershipexpiration']=$_POST['membershipexpiration'];

$reg_vars['membershiprenewal']=$_POST['membershiprenewal'];

//payment.php
$reg_vars['paymentfirstname']=$_POST['paymentfirstname'];
$reg_vars['paymentlastname']=$_POST['paymentlastname'];
$reg_vars['paymentaddress']=$_POST['paymentaddress'];
$reg_vars['paymentcity']=$_POST['paymentcity'];
$reg_vars['paymentstate']=$_POST['paymentstate'];
$reg_vars['paymentzip']=$_POST['paymentzip'];

$reg_vars['cardtype']=$_POST['cardtype'];
$reg_vars['cardnumber']=$_POST['cardnumber'];
$reg_vars['cardexpiration']=$_POST['cardexpiration'];

$reg_vars['donationamount']=$_POST['donationamount'];

$reg_vars['paymentpolicy']=isset($_POST['paymentpolicy']);

$reg_vars['registrationsummary']=$_POST['registrationsummary'];

$reg_vars['cost']=$_POST['cost'];

?>
