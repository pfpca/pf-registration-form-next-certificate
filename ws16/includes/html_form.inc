<?php
$get_section=check_plain(strtoupper($_GET["section"]));
?>

<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Registration | Pittsburgh Filmmakers</title>
    <link rel="stylesheet" href="css/foundation.min.css" />
    <style>
    dl.tabs{
      font-size: 1rem;
      line-height: 1.6;
      margin-bottom: 1.25rem !important;
      list-style-position: outside;
      font-family: inherit;
    }
    dl.tabs {
      display: block;
      height: 1.5rem;
      margin-left: -0.3125rem;
    }
    dl.tabs dd {
      height: 1.5rem;
      color: #222;
      font-size: 0.875rem;
      margin-left: 0.3125rem;
      float: left;
      display: block;
    }

    dl.tabs dd a {
      display: block;
      padding: 0.0625rem 0.625rem 0.0625rem;
      color: #999;
      -webkit-border-radius: 3px;
      border-radius: 3px;
      font-size:.875rem;
      background:none;
    }

    dl.tabs dd.active a {
      background: #008cba;
      color: #fff;
      font-weight: bold;
      cursor: default;
    }

    body{
      overflow-x:hidden;
    }

    #registrationsummary{
      background-color: #fff;
      border: 1px solid #ccc;
      -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
      box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
      color: rgba(0,0,0,0.75);
      display: block;
      font-size: 0.875rem;
      margin: 0 0 1rem 0;
      padding: 0.5rem;
      min-height: 2.3125rem;
      width: 100%;
      -moz-box-sizing: border-box;
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
    }

    .description {
      font-size: 0.85rem;
    }

    </style>
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

    <div class="row">
      <div class="large-12 columns">

        <div class="row"><!-- Breadcrumbs -->
          <div class="large-12 columns">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a></li>
              <li><a href="/education/pfm">Education</a></li>
              <li><a href="/education/pfm/classes">Classes</a></li>
              <li><a href="#"><?php print $semester_name; ?></a></li>
              <li class="current"><a href="#">Registration</a></li>
            </ul>
          </div>
        </div><!-- /Breadcrumbs -->


        <dl class="tabs" data-tab data-options="deep-linking:true" ><!-- Tabs/Panels -->
          <dd class="active"><a id="classes-tab" href="#classes-panel">Classes</a></dd>
          <dd><a id="profile-tab" href="#profile-panel">Profile</a></dd>
          <dd><a id="payment-tab" href="#panel3">Payment</a></dd>
        </dl><!-- /Tabs/Panels -->

        <h1>Pittsburgh Filmmakers Class Registration</h1>
        <h2 class="subheader"><?php print $semester_name; ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="medium-12 column">
      <?php print (theme_status_messages()); ?>
      </div>
    </div>
    <form id="form" action="index.php" method="POST" novalidate>
    <div class="row">
      <div class="large-12 columns">

        <div class="tabs-content">
          <div class="content active" id="classes-panel"><!-- Classes Panel -->

            <div class="row">
              <div class="large-12 columns">
                <div class="panel">
                  <h3>Getting Started</h3>
                  <p>Browse our <a href="https://filmmakers.pfpca.org/school/classes/schedules">class schedules</a> to find semester and class information prior to registering.</p>
                  <p>You should also read our registration guidelines to make sure you understand how courses at Pittsburgh Filmmakers work. If you have any questions, feel free to call us at 412-681-5449.</p>
                
                 <div class="row">
                 
                    
                   
                 </div>
               </div>
              </div>
            </div>          

            <div class="row">
              <div class="large-12 columns">
                <fieldset>
                  <legend>Class Registration</legend>
                  <div id="class_registration"> </div>
                  <input type="hidden" name="getSection" value="<?php print $get_section; ?>" >
                  <div class="row">
                    <div class="medium-3 columns">
                      <label>Credit Type</label>
                      <select name="creditSelect">
                        <option value="">Select Credit Type</option>
                        <option value="Certificate">Certificate</option>
                      </select>
                    </div>
                    <div class="medium-3 columns">
                      <label>Course Type</label>
                      <select name="categorySelect" onchange="typeFilter(this.value)">
                        <option value="">Select Course Type</option>
                        <option value="noprereqs">No Prerequisites</option>
                        <option value="fdv">Film Digital Video</option>
                        <option value="pht">Photography</option>
                        <option value="short">Short Courses</option>
                        <option value="indst">Independent Study</option>
                      </select>
                    </div>
                    <div class="medium-3 columns">
                      <label>Class</label>
                      <select name="classSelect" onchange="classFilter(this.value)">
                        <option value="">Select Class</option>
                      </select>
                    </div>
                    <div class="medium-3 columns">
                      <label>Section</label>
                      <select name="sectionSelect" id="section">
                        <option value="" selected>Select Section</option>
                      </select>
                    </div>
                  </div>
                </fieldset>
                <input type="button" onclick="addClass();" id="button_addclass" class="medium button round tiny" value="+ Add Class">
              </div>
            </div>

            <div class="row" id="required_prerequisites_wrapper">
              <div class="large-12 columns">
                  <fieldset>
                    <legend>Required Prerequisites</legend>
                    <div id="required_prerequisites"></div>
                  </fieldset>
              </div>
            </div>

            <div class="row">
              <div class="large-12 columns">
                  <fieldset>
                    <div id="completed_prerequisites"></div>
                    <legend>Completed Prerequisites</legend>
                    <div class="row">
                      <div class="large-4 columns">
                        <label>Course Title<input type="text" name="prereqtitle" />
                        </label>
                      </div>
                      <div class="large-4 columns">
                        <label>Instructor<input type="text" name="prereqinstructor" />
                        </label>
                      </div>
                      <div class="large-4 columns">
                        <label>Semester<input type="text" name="prereqsemester" />
                        </label>
                      </div>
                    </div>
                  </fieldset>
                <input type="button" onclick="addPrerequisite();" class="medium button round tiny" value="+ Add Completed Prerequisite">
              </div>
            </div>

            <div class="row">
              <div class="small-6 medium-8 columns right">
                <a href="#" class="button radius expand" onclick="goto_profile()">Next Step → </a>
              </div>
            </div>


          </div><!-- /Classes Panel -->
          <div class="content" id="profile-panel"> <!-- Profile Panel -->

            <!-- Profile Content -->

            <div class="row">
              <div class="large-12 columns">
                <div class="panel">
                  <h3>Membership Information</h3>
                  <p>Pittsburgh Filmmakers is part of PF/PCA, a cultural organization that includes Pittsburgh Filmmakers, Pittsburgh Center for the Arts, and Filmmakers Cinema theaters. A PF/PCA membership provides discounts and service benefits at our constituent organizations, including Pittsburgh Filmmakers, as well as reciprocal benefits at other great companies around Pittsburgh. <a href="http://pfpca.org/get-involved/membership">Learn more about PF/PCA membership</a>.</p>
                  <p>The different levels of membership are described below. Please indicate which level applies to you in the Membership field of the "Student Profile" section.</p>
                  <div class="row">
                    <div class="large-4 medium-4 columns">
                      <p>
                        <span class="bold">No Membership</span>
                        <br/>You miss out on class discounts and other membership-exclusive benefits. If you would like to take advantage of these benefits you can <a target="_blank" href="http://reg130.imperisoft.com/PittsburghArts/SelectMembership/Registration.aspx">apply for membership</a>.
                      </p>
                    </div>
                    <div class="large-4 medium-4 columns">
                      <p>
                        <span class="bold">Associate/Basic Access</span>
                        <br/>You receive a $25 discount on classes and camps at Pittsburgh Filmmakers and Pittsburgh Center for the Arts in addition to <a target="_blank" href="https://pfpca.org/get-involved/membership/associate">all our other associate/basic access benefits</a>.
                      </p>
                    </div>
                    <div class="large-4 medium-4 columns">
                      <p>
                        <span class="bold">Full Access</span>
                        <br/>You receive full equipment access in our Ceramics, Printmaking, Digital, Video, Film, Photo, and Motion Picture Fundamentals classes. <a target="_blank" href="https://pfpca.org/get-involved/membership/access">Learn all about Full Access Membership</a>.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

<div class="row">
  <div class="medium-6 columns">
    <fieldset><legend>Student Contact</legend>
      <label>First Name
        <!--input type="text" name="contactfirstname" /-->
        <?php print input('contactfirstname');?>
      </label>
      <label>Last Name
        <?php print input('contactlastname');?>
      </label>
      <label>Phone Number
        <?php print input('contactphone');?>
      </label>
      <label>Email
        <?php print input('contactemail', 'email', array('placeholder'=>"you@example.com"));?>
      </label>
      <label>Street Address
        <?php print input('contactaddress');?>
      </label>
      <label>City
        <?php print input('contactcity');?>
      </label>
      <label>State
        <!--input type="text" name="contactstate" /-->
        <select name="contactstate">
          <option value="">Select a state</option>
          <?php if(isset($reg_vars['contactstate'])){
            print form_select_options($states, $reg_vars['contactstate']);
          }else{
            print form_select_options($states);
          }
          ?>
        </select>
      </label>
      <label>Zip
        <?php print input('contactzip');?>
      </label>
    </fieldset>
  </div>
  <div class="medium-6 columns">
    <fieldset><legend>Student Profile</legend>
      <label>HS Grad Year
        <?php print input('hsgradyear');?>
      </label>
      <label>Date of Birth
        <?php print input('dateofbirth', 'date');?>
      </label>
      <label>Membership</label>
      <select name="profilemembership">
        <option value="">No Membership</option>
        <?php if(isset($reg_vars['profilemembership'])){
          print form_select_options($memberships, $reg_vars['profilemembership']);
        }else{
          print form_select_options($memberships);
        }
        ?>
        <!--option value="Basic/Associate Member">Basic/Associate Member</option>
        <option value="Full Access Member">Full Access Member</option-->
      </select>
      <label id="membershipexpiration">Membership Expiration
        <?php print input('membershipexpiration', 'date', array('placeholder'=>"YYYY/MM/DD"));?>
      </label>
      <label id="membershiprenewal">Membership Renewal
        <select name="membershiprenewal">
          <option value="">No Renewal</option>
          <?php if(isset($reg_vars['membershiprenewal'])){
            print form_select_options($membershiprenewals, $reg_vars['membershiprenewal']);
          }else{
            print form_select_options($membershiprenewals);
          }
          ?>
        </select>
      </label>
    </fieldset>

  </div>
</div>


            <!-- /Profile Content -->
            <div class="row">
              <div class="small-6 medium-4 columns">
                <a href="#" class="button radius expand secondary" onclick="goto_classes()">Back</a>
              </div>
              <div class="small-6 medium-8 columns">
                <a href="#" class="button radius expand" onclick="goto_payment()">Next Step → </a>
              </div>
            </div>
          </div> <!-- /Profile Panel -->
          <div class="content" id="panel3"> <!-- Payment Panel -->
            <!-- Payment Content -->

            <div class="row">
              <div class="medium-6 columns">
                <fieldset><legend>Payment Info</legend>
                <span class="right" style="cursor:pointer" onclick="copyContact()">Copy from student contact</span>
                  <label>Credit Card First Name
                    <?php print input('paymentfirstname');?>
                  </label>
                  <label>Credit Card Last Name
                    <?php print input('paymentlastname');?>
                  </label>
                  <label>Street Address
                    <?php print input('paymentaddress');?>
                  </label>
                  <label>City
                    <?php print input('paymentcity');?>
                  </label>
                  <label>State
                    <?php print input('paymentstate');?>
                  </label>
                  <label>Zip
                    <?php print input('paymentzip');?>
                  </label>
                  <label>Card Type</label>
                  <select name="cardtype" >
                    <option value="">Select Card Type</option>
                    <?php if(isset($reg_vars['cardtype'])){
                      print form_select_options($cardtypes, $reg_vars['cardtype']);
                    }else{
                      print form_select_options($cardtypes);
                    }
                    ?>
                  </select>
                  <label>Card Number
                    <?php print input('cardnumber');?>
                  </label>
                  <label>Card Expiration
                    <?php print input('cardexpiration');?>
                  </label>

                </fieldset>
              </div>
              <div class="medium-6 columns">
                <fieldset><legend>Donation</legend>
                  <label>Donation Amount
                    <?php print input('donationamount');?>
                  </label>
                  <p>The Pittsburgh Filmmakers Scholarship Fund provides financial support to new and continuing low-income students who wish to study at Pittsburgh Filmmakers. If you would like to support students' study of film at our facilities, you may include a donation of any amount in the field above.</p>
                </fieldset>

                <fieldset><legend>Terms of Payment</legend>
                  <label>Registration Summary</label>
                  <div id="registrationsummary">

                  </div>
                  <textarea name="registrationsummary" style="display:none" readonly>

                  </textarea>
                  <label>Total Cost
                    <input type="text" name="cost" readonly >
                  </label>
                  <p>
                 Refunds of fees paid by accepted Certificate Program students will be handled as follows:<br><br>If dropping before the first class meeting 100% of tuition for the term is refundable. If dropping or withdrawing during the first seven (7) calendar days of the semester, 75% of the tuition for the term, semester or quarter is refundable. If dropping or withdrawing after the first seven (7) calendar days, but within the first 25% of the semester, 55% of the tuition for the term, semester or quarter is refundable. If dropping or withdrawing after 25% but within 50% of the semester, 30% of the tuition is refundable. If dropping or withdrawing after 50% of the semester, the student is not entitled to a refund.<br><br>A student who is entitled to a refund must submit a signed Drop form to the Registrar (see Drop policy). Refund calculation will be based on the last day of recorded attendance. Students who notify the Registrar of their wish to withdraw from a class will receive a grade of "W" (students who do not notify the Registrar risk receiving a grade of "F"). All eligible refunds will be issued within thirty (30) calendar days of the Registrar’s receipt of a signed Drop form.
                  </p>
                  <label for="paymentpolicy"><input type="checkbox" name="paymentpolicy" id="paymentpolicy" value="Agree" <?php if($reg_vars['paymentpolicy']){print "checked";} ?>> &nbsp;I have read, understand, and agree to the above refund policy.</label>
                </fieldset>
              </div>
            </div>


            <!-- /Payment Content -->
            <div class="row">
              <div class="small-6 medium-4 columns">
                <a href="#" class="button radius expand secondary" onclick="goto_profile()">Back</a>
              </div>
              <div class="small-6 medium-8 columns">
                <input type="submit" class="button radius expand" value="Complete Registration"/>
              </div>
            </div>
          </div> <!-- /Payment Panel -->
        </div>

      </div>
    </div>

    </form>




    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="registration.js"></script>
    <script>
      $(document).foundation();
    </script>
    <script src="js/calculation.js"></script>
    <script>
    regobj=registration_object;
    classes_list=regobj.classes;

    function goto_classes(){
      $('#classes-tab').trigger('click');
      $('body').scrollTop(0);
    }


    function goto_profile(){
      $('#profile-tab').trigger('click');
      $('body').scrollTop(0);
    }


    function goto_payment(){
      $('#payment-tab').trigger('click');
      $('body').scrollTop(0);
    }


    function goto_complete(){
      $('#complete-tab').trigger('click');
      $('body').scrollTop(0);
    }

    /* Conditional Display Functions */
    function memExpDisplay(){
      if($('[name=profilemembership]').val()==''){
        $('#membershipexpiration').hide();
        $('[name=membershipexpiration]').val("").change();
      }
      else{
        $('#membershipexpiration').show();
      }
    }


    function memRenewDisplay(){
      memExp=$("[name=membershipexpiration]").val();
      if(memExp!="" && Date.parse(memExp)<Date.parse(regobj.dates.memExtendsThru)){
        $('#membershiprenewal').show();
      }
      else{
        $('#membershiprenewal').hide();
        $('[name=membershiprenewal]').val("");
      }
    }


    /* /Conditional Display Functions */


    var class_rows=0;
    var prereq_rows=0;

    function titleFromCid(cid){
      title="";
      $.each(classes_list, function(index, c_item){
        if(c_item.cid==cid){
          title=c_item.title;
        }
      });
      return title;
    }

    function addClass(){
      credit=$('[name=creditSelect]').val();
      section=$('[name=sectionSelect]').val();
      cid=section.slice(0,-1);
      courseTitle=titleFromCid(cid);
      class_row_id="class-row-"+class_rows;
      field_prefix='<div id="'+class_row_id+'" class="row collapse added-class">';
      credit_prefix='<div class="small-3 columns"><input name="credit[]" type="text" value="';
      credit_suffix='" readonly></div>';
      title_prefix='<div class="small-4 columns"><input name="title[]" type="text" value="';
      title_suffix='" readonly></div>';
      section_prefix='<div class="small-3 columns"><input name="section[]" type="text" value="';
      section_suffix='" readonly></div>';
      field_suffix='<div class="small-2 columns"><input type="button" onclick="removeClass(\''+class_row_id+'\')" title="Remove class" class="button_removeclass button secondary postfix" value="X"></div></div>';
      if(credit==""){
        alert('Please select a credit type');
      }
      else if(section==""){
        alert('Please select a section');
      }
      else{
        $('#class_registration').append(field_prefix+credit_prefix+credit+credit_suffix+title_prefix+courseTitle+title_suffix+section_prefix+section+section_suffix+field_suffix);
        class_rows++;
        typeFilter('');
        $('[name=categorySelect]').val('');
        refreshReqs();
      }

    }



    function removeClass(id){
      selector="#"+id;
      $(selector).remove();
      refreshReqs();
      calculateCost();
    }

    function refreshReqs(){
      $('#required_prerequisites').html('');
      $('#required_prerequisites_wrapper').hide();
      requiredReqs=false;

      $('[name="section[]"]').each(function(){
        section=$(this).val();
        cid=section.slice(0,-1);
        $.each(classes_list, function(index, c_item){
          if(c_item.cid==cid){
            requiredReqs=true;
            $('#required_prerequisites').append('<p>'+cid+" : "+c_item.prereqs+'</p>');
          }
        });
      });
      if(requiredReqs){
        $('#required_prerequisites_wrapper').show();
      }
    }

    function addPrerequisite(){
      prereq_title=$('[name=prereqtitle]').val();
      prereq_fac=$('[name=prereqinstructor]').val();
      prereq_semester=$('[name=prereqsemester]').val();

      prereq_row_id="prereq-row-"+prereq_rows;
      field_prefix='<div id="'+prereq_row_id+'" class="row collapse added-class"><div class="small-10 columns"><input type="text" name="prereq[]" value="';

      field_suffix='" readonly></div><div class="small-2 columns"><input type="button" onclick="removePrerequisite(\''+prereq_row_id+'\')" title="Remove class" class="button secondary postfix" value="X"></div></div>';

      if(prereq_title==""){
        alert('Please at least fill in the prerequisite title before adding.');
      }
      else{
        $('#completed_prerequisites').append(field_prefix+prereq_title+':'+prereq_fac+":"+prereq_semester+field_suffix);
        prereq_rows ++;
        clearPrerequisite();
      }
    }


    function removePrerequisite(id){
      selector="#"+id;
      $(selector).remove();
    }


    function clearPrerequisite(){
      $('[name=prereqtitle]').val('');
      $('[name=prereqinstructor]').val('');
      $('[name=prereqsemester]').val('');
    }


    function typeFilter(medium){
      classSelect=$('[name=classSelect]');
      sectionSelect=$('[name=sectionSelect]');
      classSelect.empty().append($("<option></option>").attr("value","").text("Select Class"));
      sectionSelect.empty().append($("<option></option>").attr("value","").text("Select Section"));
      if(medium!=""){
        $.each(classes_list,function(index,c_item){
          if($.inArray(medium, c_item.mediums)>-1){
            //update classes
            classSelect.append($("<option></option>")
            .attr("value",c_item.cid)
            .text(c_item.title));
            //update sections
            $.each(c_item.sections, function(index,s_item){
              sectionSelect.append($("<option></option>")
              .attr("value",s_item.sid)
              .text(s_item.title));
            });
          }
        });
      }
      else{//add all options
        $.each(classes_list,function(index,c_item){ //class item
          //update classes
          classSelect.append($("<option></option>")
         .attr("value",c_item.cid)
         .text(c_item.title));
         $.each(c_item.sections, function(index,s_item){
              sectionSelect.append($("<option></option>")
              .attr("value",s_item.sid)
              .text(s_item.title));
            });
        });
      }
    }


    function classFilter(cid){
      sectionSelect=$('[name=sectionSelect]');
      sectionSelect.empty().append($("<option></option>").attr("value","").text("Select Section"));
      if(cid!=""){
        $.each(classes_list,function(index,c_item){
          if(cid==c_item.cid){
            //update sections
            $.each(c_item.sections, function(index,s_item){
              sectionSelect.append($("<option></option>")
              .attr("value",s_item.sid)
              .text(s_item.title));
            });
          }
        });
      }
      else{
        typeFilter($('[name=categorySelect]').val());
      }

    }


    function copyContact(){
      $('[name=paymentfirstname]').val(
        $('[name=contactfirstname]').val()
      );
      $('[name=paymentlastname]').val(
        $('[name=contactlastname]').val()
      );
      $('[name=paymentaddress]').val(
        $('[name=contactaddress]').val()
      );
      $('[name=paymentcity]').val(
        $('[name=contactcity]').val()
      );
      $('[name=paymentstate]').val(
        $('[name=contactstate]').val()
      );
      $('[name=paymentzip]').val(
        $('[name=contactzip]').val()
      );
    }


    //init
    typeFilter("");
    getSection=$('[name=getSection]').val();
    if(getSection!=''){
      $('[name=sectionSelect] option').each(function(){
        if ($(this).val() == getSection) {
          $(this).attr("selected","selected");
        }
      });
    }

    refreshReqs();


    //catch forgot-to-add values
    $('#form').on('submit', function(){
        if($('[name=sectionSelect]').val()!=''){
          addClass($('#form'));
        }
    });

    //attach and inits

    $('[name=profilemembership]').on('change', function(){
      memExpDisplay();
      calculateCost();
    });memExpDisplay();

    $("[name=membershipexpiration]").on('change',function(){
      memRenewDisplay();
      calculateCost();
    });memRenewDisplay();
    //membership expiration onchange call function()checkMemStatus.

    $("[name=membershiprenewal]").on('change',function(){
      calculateCost();
    });

    $("#button_addclass").on('click',function(){
      calculateCost();
    });

    $('[name=donationamount]').on('change', function(){
      calculateCost();
    });

    calculateCost();
    </script>
  </body>
</html>
