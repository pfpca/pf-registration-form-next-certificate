<?php
//ready for final processing
//send out the email/display results
$body="";
$body.="Classes: \n";
for($i=0; $i<sizeof($reg_vars['section']); $i++){
$body.="\t".check_plain($reg_vars['credit'][$i])." : ".check_plain($reg_vars['title'][$i])." : ".check_plain($reg_vars['section'][$i])."\n";
}
$body.="Taken Prerequisites: \n";
for($i=0; $i<sizeof($reg_vars['prereq']); $i++){
  $body.="\t".check_plain($reg_vars['prereq'][$i])."\n";
}
$body .= "First Name: ".$reg_vars['contactfirstname']."\n".
"Last Name: ".$reg_vars['contactlastname']."\n".
"Phone Number: ".$reg_vars['contactphone']."\n".
"Email: ".$reg_vars['contactemail']."\n".
"Address: ".$reg_vars['contactaddress']."\n".
"City: ".$reg_vars['contactcity']."\n".
"State: ".$reg_vars['contactstate']."\n".
"Zip: ".$reg_vars['contactzip']."\n".
"HS Grad: ".$reg_vars['hsgradyear']."\n".
"Birthdate: ".$reg_vars['dateofbirth']."\n";
if(!empty($reg_vars['profilemembership'])){
  $body.="Membership: ".$reg_vars['profilemembership']."\n";
}
if(!empty($reg_vars['membershipexpiration'])){
  $body.="Membership Expiration: ".$reg_vars['membershipexpiration']."\n";
}
if(!empty($reg_vars['membershiprenewal'])){
  $body.="Membership Renewal: ".$reg_vars['membershiprenewal']."\n";
}
if(!empty($reg_vars['paymentfirstname'])){
  $body.="Payment First Name: ".$reg_vars['paymentfirstname']."\n";
}
if(!empty($reg_vars['paymentlastname'])){
  $body.="Payment Last Name: ".$reg_vars['paymentlastname']."\n";
}
if(!empty($reg_vars['cardtype'])){
  $body.="Card Type: ".$reg_vars['cardtype']."\n";
}
if(!empty($reg_vars['cardexpiration'])){
  $body.="Card Expiration: ".$reg_vars['cardexpiration']."\n";
}
if(!empty($reg_vars['donationamount'])){
  $body.="Donation Amount: $".$reg_vars['donationamount']."\n";
}
if(!empty($reg_vars['cost'])){
  $body.="Total Cost $".$reg_vars['cost']."\n";
}
if(!empty($reg_vars['registrationsummary'])){
  $body.="Registration Summary:\n".$reg_vars['registrationsummary']."\n";
}

  $staff_mail = "dory@pghfilmmakers.org";
  $staff_mail2 = "joeb@pghfilmmakers.org";
  $staff_mail3 = "onlinereg@pghfilmmakers.org";
  $from_mail = "online@pghfilmmakers.org";

  $subject = "PFM Course Registration - ".$semester_name;

  ini_set("sendmail_from","online@pghfilmmakers.org");

  mail($staff_mail, $subject, $body, "From: $from_mail");
  mail($staff_mail2, $subject, $body, "From: $from_mail");
  mail($staff_mail3, $subject, $body, "From: $from_mail");
  //mail($testEmail1, $subject, $body, "From: $fromAdd");
  mail($reg_vars['contactemail'], $subject, $body, "From: $from_mail");



?>
<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Complete | PFM Registration</title>
    <link rel="stylesheet" href="css/foundation.min.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <div class="row">
      <div class="large-12 columns">
        <div class="row">
        <div class="large-12 columns">
          <ul class="breadcrumbs">
            <li><a href="#">Home</a></li>
            <li><a href="#">Education</a></li>
            <li><a href="#">Classes</a></li>
            <li><a href="#"><?php print $semester_name; ?></a></li>
            <li class="current"><a href="#">Registration</a></li>
          </ul>
        </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <ul class="pagination">
              <li class="unavailable"><a>Classes</a></li>
              <li class="unavailable"><a>Profile</a></li>
              <li class="unavailable"><a>Payment</a></li>
              <li class="current"><a>Complete</a></li>
            </ul>
          </div>
        </div>

        <h1>PFM.edu Course Registration</h1>
        <h2 class="subheader"><?php print $semester_name; ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="large-12 columns">
        <?php print (theme_status_messages()); ?>
      </div>
    </div>

    <div class="row">
      <div class="large-12 column">
        <div class="panel">
          <h2>Thank you for Registering!</h2>
          <p class="lead">Check your e-mail for a copy of your registration submission.  A confirmation letter will arrive in the mail <strong><em>once your registration is confirmed</em></strong>. </p>
          <p><a class="button" href='http://pfm.pittsburgharts.org/education/pfm'>Go Back to Filmmakers Education</a></p>
        </div>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
