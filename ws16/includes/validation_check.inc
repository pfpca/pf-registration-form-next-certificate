<?php

    if(empty($reg_vars['section'])){
      set_form_error("You need to select classes to register for. Make sure to click the '+ Add Class' button.");
    }

    if(empty($reg_vars['contactfirstname'])){
      set_form_error('You need to fill out the contact first name field.');
    }

    if(empty($reg_vars['contactlastname'])){
      set_form_error('You need to fill out the contact last name field.');
    }

    if(empty($reg_vars['contactphone'])){
      set_form_error('You need to fill out the contact phone number field.');
    }

    if(empty($reg_vars['contactemail']) || !valid_email_address($reg_vars['contactemail'])){
      set_form_error('please enter a valid email address');
    }

    if(empty($reg_vars['contactaddress'])){
      set_form_error('You need to fill out the contact address field.');
    }

    if(empty($reg_vars['contactcity'])){
      set_form_error('You need to fill out the contact city field.');
    }

    if(empty($reg_vars['contactstate'])){
      set_form_error('You need to select a state for your contact info.');
    }elseif(!valid_state($reg_vars['contactstate'])){
      set_form_error('Please enter a valid state.');
    }

    if(empty($reg_vars['contactzip'])){
      set_form_error('You need to fill out the contact zip field.');
    }elseif(!valid_zip($reg_vars['contactzip'])){
      set_form_error('Please enter a valid zip code.');
    }


    if(empty($reg_vars['hsgradyear'])){
      set_form_error('You need to fill out the graduation year field.');
    }elseif(!valid_year($reg_vars['hsgradyear'])){
      set_form_error('Please enter a valid graduation year.');
    }


    if(empty($reg_vars['dateofbirth'])){
      set_form_error('You need to fill out the date of birth field.');
    }elseif(!valid_dob($reg_vars['dateofbirth'])){
      set_form_error('Please enter a valid date of birth.');
    }




    if(!empty($reg_vars['profilemembership']) && !valid_membership_type($reg_vars['profilemembership'])){
      set_form_error('An error occurred with your membership type selection.');
    }


    if(!empty($reg_vars['profilemembership'])){
      if(empty($reg_vars['membershipexpiration'])){
        set_form_error('You need to fill out the membership expiration field.');
      }elseif(!valid_membership_expiration($reg_vars['profilemembership'])){
        set_form_error('Please enter a valid date for your membership expiration.');
      }
    }


    if(!empty($reg_vars['membershiprenewal']) && !valid_membership_renewal($reg_vars['membershiprenewal'])){
      set_form_error('An error occurred with your membership renewal selection.');
    }


    if(empty($reg_vars['paymentfirstname'])){
      set_form_error('Please enter a valid payment first name.');
    }

    if(empty($reg_vars['paymentlastname'])){
      set_form_error('Please enter a valid payment last name.');
    }


    if(empty($reg_vars['paymentaddress'])){
      set_form_error('Please enter a valid payment address.');
    }


    if(empty($reg_vars['paymentcity'])){
      set_form_error('Please enter a payment city.');
    }


    if(!valid_payment_state($reg_vars['paymentstate'])){
      set_form_error('Please enter a valid payment state.');
    }


    if(!valid_payment_zip($reg_vars['paymentzip'])){
      set_form_error('Please enter a valid payment zip.');
    }


    if(empty($reg_vars['cardtype'])){
      set_form_error('Please select a payment card type.');
    }elseif(!valid_payment_cardtype($reg_vars['cardtype'])){
      set_form_error('Please enter a valid payment card type.');
    }



    if(empty($reg_vars['cardnumber'])){
      set_form_error('Please enter a payment card number.');
    }elseif(!valid_payment_cardnumber($reg_vars['cardnumber'])){
      set_form_error('Please enter a valid payment card number.');
    }


    if(empty($reg_vars['cardexpiration'])){
      set_form_error('Please enter a valid payment card expiration');
    }


    if(!valid_donationamount($reg_vars['donationamount'])){
      set_form_error('Your donation amount is invalid.');
    }


    if(!$reg_vars['paymentpolicy']){
      set_form_error('You must accept the payment policy to register.');
    }


    if(!valid_cost($reg_vars['cost'])){
      set_form_error('There was an error with the cost submitted.');
    }


?>
