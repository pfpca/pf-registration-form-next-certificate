<?php

$states = array ("AL" => "Alabama", "AK" => "Alaska", "AZ" => "Arizona",
  "AR" => "Arkansas", "CA" => "California", "CO" => "Colorado",
  "CT" => "Connecticut", "DE" => "Delaware",
  "DC" => "District of Columbia", "FL" => "Florida",
  "GA" => "Georgia", "HI" => "Hawaii", "ID" => "Idaho",
  "IL" => "Illinois", "IN" => "Indiana", "IA" => "Iowa",
  "KS" => "Kansas", "KY" => "Kentucky", "LA" => "Louisiana",
  "ME" => "Maine", "MD" => "Maryland", "MA" => "Massachusetts",
  "MI" => "Michigan", "MN" => "Minnesota", "MS" => "Mississippi",
  "MO" => "Missouri", "MT" => "Montana", "NE" => "Nebraska",
  "NV" => "Nevada", "NH" => "New Hampshire", "NJ" => "New Jersey",
  "NM" => "New Mexico", "NY" => "New York",
  "NC" => "North Carolina", "ND" => "North Dakota",
  "OH" => "Ohio", "OK" => "Oklahoma", "OR" => "Oregon",
  "PA" => "Pennsylvania", "RI" => "Rhode Island",
  "SC" => "South Carolina", "SD" => "South Dakota",
  "TN" => "Tennessee", "TX" => "Texas", "UT" => "Utah",
  "VT" => "Vermont", "VA" => "Virginia", "WA" => "Washington",
  "WV" => "West Virginia", "WI" => "Wisconsin",
  "WY" => "Wyoming");

$memberships = array ("Basic/Associate Member" => "Basic/Associate Member", "Full Access Member" => "Full Access Member");

$cardtypes = array ("visa"=> "Visa", "mastercard"=>"MasterCard", "discover"=>"Discover", "amex"=>"American Express");

$membershiprenewals = array ("Individual Associate"=>"Individual Associate ($60)", "Family Associate"=>"Family Associate ($90)", "Individual Basic Access"=>"Individual Basic Access ($75)");

function input($name, $type='text', $attrs=NULL){
  global $reg_vars;
  if(isset($reg_vars[$name])){
    $value=check_plain($reg_vars[$name]);
  }else{$value='';}
  $attributes='';
  if(!empty($attrs)){
    foreach($attrs as $key => $val){
      $attributes.=" $key=\"$val\"";
    }
  }
  $input='<input type="'.$type.'" name="'.$name.'" value="'.$value.'" '.$attributes.' />';
  return $input;
}

function check_plain($text) {
  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function set_form_error($message=''){
  global $form_error;
  $form_error=true;
  set_message(check_plain($message), 'error');
}

function restore($name){
  global $reg_vars;
  if(isset($reg_vars[$name])){
    return check_plain($reg_vars[$name]);
  }
}


function form_select_options($choices, $value=NULL){
  $options="";
  foreach ($choices as $key => $choice) {
    if($key===$value){
      $selected=' selected="selected"';
    }else{
      $selected='';
    }
    $options .= '<option value="' . check_plain($key) . '"' . $selected . '>' . check_plain($choice) . '</option>';
  }
  return $options;
}


$messages=array();
function set_message($message= NULL, $type='status'){
  global $messages;
  if (!isset($messages[$type])) {
      $messages[$type] = array();
  }
  $messages[$type][]=$message;
}

function theme_status_messages() {
  global $messages;
  $output = '';

  foreach ($messages as $type => $message_list) {
    $type_class='';
    switch($type):
      case 'status':
        $type_class='info';
        break;
      case 'error':
        $type_class='alert';
        break;
    endswitch;
    $output .= "<div data-alert class=\"alert-box $type_class\">\n";
    if (count($message_list) > 1) {
      $output .= " <ul>\n";
      foreach ($message_list as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $message_list[0];
    }
    $output .= "<a href=\"#\" class=\"close\">&times;</a></div>\n";
  }
  return $output;
}

function valid_credit($value){
  set_message($value, 'status');
  return (bool) in_array($value, array('Non-Credit','Credit','Certificate'));
}

function valid_section($value){
  //generate array from json data
  //return (bool in_array($value, $sections));
  return true;//TODO write proper return function
}


function valid_email_address($value) {
  return (bool) filter_var($value, FILTER_VALIDATE_EMAIL);
}


function valid_state($value){
  return (bool) preg_match('/^(?:A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])*$/', $value);
}


function valid_zip($value){
  return (bool) preg_match('/^\d{5,6}(?:[-\s]\d{4})?$/', $value);
}


function valid_year($value){
  return (bool) preg_match('/^\d{4}$/', $value);
}


function valid_dob($value){
  return true; //TODO write proper function
}


function valid_membership_type($value){
  return (bool) in_array($value, array('Basic/Associate Member','Full Access Member'));
}


function valid_membership_expiration($value){
  return true; //TODO write proper function
}


function valid_membership_renewal($value){
  return (bool) in_array($value, array('Individual Associate','Family Associate','Individual Basic Access'));
}


function valid_teacherid($value){
  return (bool) preg_match('/^\d{7}$/', $value);
}


function valid_payment_name($value){
  return true; //TODO write proper function
}


function valid_payment_address($value){
return true; //TODO write proper function
}


function valid_payment_city($value){
return true; //TODO write proper function
}


function valid_payment_state($value){
  return true; //TODO write proper function
}


function valid_payment_zip($value){
  return true; //TODO write proper function
}


function valid_payment_cardtype($value){
  return true; //TODO write proper function
}


function valid_payment_cardnumber($value){
  return true; //TODO write proper function
}


function valid_payment_cardexpiration($value){
  return true; //TODO write proper function
}


function valid_donationamount($value){
  return true; //TODO write proper function
}


function valid_cost($value){

  return !empty($value);
}


?>
