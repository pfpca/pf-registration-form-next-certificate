
/*
independent non-certificate: 390 per course
  plus course fee
  deadline discount to $350 per course before deadline
  membership discounts apply

certificate: 390 per course.
  no fee.
  no discounts ever.

Credit transcript: 1120 per 42 classroom hour
  plus course fee
  deadline discounts?
  membership discounts?

membership discounts $25 off first course
and $35 off additional courses (not
certificate courses.) Access members get
one course fee waiver.


*/

/*
reset cost. this will be updated as certain items updated.

get membership variables.

loop through credit/courses.
depending on credit types, do calculation for courses.
  check against a list of exceptions

return final price to cost.

*/
/*
items to trigger calculate:
* add class
* remove class
* membership items change
* donation amount changed
*/

/*Things to ask kathleen:
 * do credit certificate students pay course fee?
 *
 */
function isException(cid){
  return cid in registration_object.prices.exceptions;
}

function calculateException(creditType, cid){

}

function calculateCost(){
  resetCost();
  resetSummary();
  // Discount Date
  discount_date=false;
  today=new Date();
  if(today <= Date.parse(registration_object.dates.discountDeadline)){
    discount_date=true;
  }
  // /Discount Date

  // Membership
	discount_membership=false;
  access_membership=false;
	membership=$("[name=profilemembership]").val();
	if(membership!=""){
		memExp=$("[name=membershipexpiration]").val();
    if(memExp!="" && Date.parse(memExp)>Date.parse(regobj.dates.memExtendsThru)){
      discount_membership=true;
      if(membership=="Full Access Member"){
        access_membership=true;
      }
    }
    else{
      memRenew=$("[name=membershiprenewal]").val();
      if(memRenew!=""){
        discount_membership=true;
        if(memRenew=="Individual Associate"){
          addCost(60, "Renew Individual Associate Membership");
        }else if(memRenew=="Family Associate"){
          addCost(90, "Renew Family Associate Membership");
        }else if(memRenew=="Individual Basic Access"){
          addCost(75, "Individual Basic Access");
        }else{//invalid
          discount_membership=false;
        }
      }
    }
	}
  // /Membership

  // Classes
  $('[name="credit[]"]').each(function(index){
    credit=$(this).val();
    section=$($('[name="section[]"]').get(index)).val();
    course=section.slice(0,-1);
    console.log(course);
    console.log(isException(course));
    if(!isException(course)){// Not exceptions
      if(credit=="Non-Credit"){
          addCost(registration_object.prices.creditTypes.nonCredit,credit+" : "+section);
          if(discount_date){
            addCost(-40,"--early registration discount");
          }
          if(discount_membership){
            addCost(-25,"--membership class discount");
          }
          feeAmount=courseFee(course);
          addCost(feeAmount,"--course fee");

      }else if(credit=="Certificate"){
        addCost(registration_object.prices.creditTypes.certificate,credit+" : "+section);
      }else{//credit transcript
        addCost(registration_object.prices.creditTypes.credit,credit+" : "+section);
        feeAmount=courseFee(course);
        addCost(feeAmount,"--course fee");
      }

    }else{
      if(credit=="Non-Credit"){
        exception=registration_object.prices.exceptions[course].nonCredit;
        addCost(exception.base,credit+" : "+section);
      }else if(credit=="Credit"){
        exception=registration_object.prices.exceptions[course].credit;
        addCost(exception.base,credit+" : "+section);
      }else if(credit=="Certificate"){
        exception=registration_object.prices.exceptions[course].certificate;
        addCost(exception.base,credit+" : "+section);
      }else{console.log('creditType error');}
    }
  });

  // /Classes

  // Donation
  donation=parseFloat($('[name=donationamount]').val());
  // /Donation
  if(donation>0){
    addCost(donation,"Donation");
  }
}

function resetCost(){
  $("[name=cost]").val(0);
}

function resetSummary(){
  $("#registrationsummary").html('');
  $("[name=registrationsummary]").val('');
  if($('[name="credit[]"]').length==0){
	 summary="<div style='color:red'>You must add a class/section. Make sure to click \'+ Add Class\'.</div>";
	 $("#registrationsummary").append(summary);
  }
}


function addCost(num, message){
  curCost=parseFloat($("[name=cost]").val());
  curCost+=num;
  $("[name=cost]").val(curCost);
  summary="<div class='row'><div class='small-2 column'>"+num+"</div><div class='small-10 column'>"+message+"</div></div>";
  $("#registrationsummary").append(summary);
  textarea=$("[name=registrationsummary]").val();
  textarea+=num+'\t'+message+'\n'
  $("[name=registrationsummary]").val(textarea);
}

function courseFee(cid){
  classes=registration_object.classes;
  for(i=0;i<classes.length;i++){
    if(classes[i].cid==cid){
      return parseFloat(classes[i].fee);
    }
  }
}
