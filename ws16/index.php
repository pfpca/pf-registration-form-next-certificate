<?php
//always loads
$semester_name="Certificate 2018";

require_once(dirname(__FILE__).'/includes/validation_functions.inc');

function getSelf() {
  $self = $_SERVER['PHP_SELF'];
  if (substr($self, -9) === "index.php") {
    $self = substr($self, 0, -9);
  }
  return $self;
}

// Make sure SSL is used
if ($_SERVER['HTTPS'] != "on") {
  $url = "filmmakers.pfpca.org";
  $path = getSelf();
  header("Location: https://$url$path");
}

//if submitted
if ($_SERVER["REQUEST_METHOD"] == "POST"):
  //receive POST data
  require_once(dirname(__FILE__).'/includes/post_receive.inc');
  //check for data errors
  $form_error=false;

  require_once(dirname(__FILE__).'/includes/validation_check.inc');

  if(!$form_error){//no errors. process
    //check registration
    require_once(dirname(__FILE__).'/includes/process_check.inc');
    if(!$form_error){
      require_once(dirname(__FILE__).'/includes/process_registration.inc');
    }else{
      require_once(dirname(__FILE__).'/includes/html_form.inc');
    }
  }else{//errors, return to form
    require_once(dirname(__FILE__).'/includes/html_form.inc');
  }

//not submitted present form
else:
  require_once(dirname(__FILE__).'/includes/html_form.inc');
endif;

?>
