var registration_object={
  dates:{
    discountDeadline:"August 11, 2018",
    memExtendsThru:"August 11, 2018",
   
  },
  prices:{
    creditTypes:{
      nonCredit:615,
      credit:1150,
      certificate:480
    },
    exceptions:{
      FLM140:{
      	nonCredit:{
      		base:275,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	credit:{
      		base:767,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	certificate:{
      		base:275
      	}
      },
      SHORT123:{
      	nonCredit:{
      		base:615,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	credit:{
      		base:1120,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	certificate:{
      		base:615
      	}
      },
      SHORT001:{
      	nonCredit:{
      		base:150,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	credit:{
      		base:373.34,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	certificate:{
      		base:150
      	}
      },
      SHORT002:{
      	nonCredit:{
      		base:150,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	credit:{
      		base:373.34,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	certificate:{
      		base:150
      	}
      },
      SHORT003:{
      	nonCredit:{
      		base:150,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	credit:{
      		base:373.34,
      		membershipDiscount:false,
      		dateDiscount:false
      	},
      	certificate:{
      		base:150
      	}
      }
    }
  },
  /* classes */
  //"noprereqs","fdv", "pht", "short", "indst"
  classes:[
  {
  title:"Filmmaking 1: Fundamentals",
  cid:"PFM101",
  fee:70,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv", "pht"],
  sections:[
    {title:"PFM101A : Mon 6:00-9:00 Moore",sid:"PFM101A"},
    {title:"PFM101B : Mon 2:00-5:00 Bonev",sid:"PFM101B"},
    {title:"PFM101C : Tue 6:00-9:00 Abrams",sid:"PFM101C"},
	{title:"PFM101D : Thur 2:00-5:00 Bugaj",sid:"PFM101D"},
	{title:"PFM101E : Thur 6:00-9:00 Caetano",sid:"PFM101E"},
	{title:"PFM101F : Fri 10:30-1:30 Zavala",sid:"PFM101F"},
  {title:"PFM101G : Sat 10:30-1:30 Downes",sid:"PFM101G"}
    ]
  },
  {
  title:"Filmmaking 2: Sight and Sound",
  cid:"PFM201",
  fee:70,
  prereqs:"Prerequisite: Filmmaking 1 (Motion Picture Fundamentals). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM201A : Wed 6:00-9:00 Koleck",sid:"PFM201A"},
	{title:"PFM201B : Tue 2:00-5:00 Wormsley",sid:"PFM201B"},
    ]
  },
  {
  title:"Filmmaking 3: Story to Screen",
  cid:"PFM301",
  fee:70,
  prereqs:"Prerequisite: Filmmaking 2 (Video Production 1). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM301A : Tue 6:00-9:00 Cantine",sid:"PFM301A"},
	]
  },
  {
  title:"Filmmaking 4: Capstone",
  cid:"PFM401",
  fee:70,
  prereqs:"Prerequisite: Filmmaking 3 (Video Production 2). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM401A : Wed 6:00-9:00 Cantine",sid:"FLM308A"},
    ]
  },
  {
  title:"Post-Production 1",
  cid:"PFM102",
  fee:70,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM102A : Tue 6:00-9:00 Hosking",sid:"PFM102A"},
    ]
  },
  {
  title:"Post-Production 2",
  cid:"PFM202",
  fee:70,
  prereqs:"Prerequisite: Post-Production 1 OR Filmmaking 1 (Intro to Digital Editing OR Motion Picture Fundamentals). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM202A : Wed 6:00-9:00 Mougianis",sid:"PFM202A"}
    ]
  },
  {
  title:"Animation 1",
  cid:"PFM103",
  fee:70,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM103A : Mon 6:00-9:00 Schwab",sid:"PFM103A"},
    ]
  },
  {
  title:"Super-8 and 16mm Film Production",
  cid:"PFM212",
  fee:70,
  prereqs:"Prerequisite: Filmmaking 1 (Motion Picture Fundamentals). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM212A : Tue 6:00-9:00 Bonello",sid:"PFM212A"},
    ]
  },
  {
  title:"Directing Motion Pictures",
  cid:"PFM208",
  fee:35,
  prereqs:"Prerequisite: Filmmaking 1 (Motion Picture Fundamentals). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM208A : Wed 6:00-9:00 Watt",sid:"PFM208A"},
	]
  },
  {
  title:"Producing Motion Pictures",
  cid:"PFM210",
  fee:35,
  prereqs:"Prerequisite: Filmmaking 1 (Motion Picture Fundamentals). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM210A : Mon 6:00-9:00 Turich",sid:"PFM210A"},
	]
  },
  {
  title:"Voice-Over Acting",
  cid:"PFM152",
  fee:35,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM152A : Mon 6:00-9:00 Bailey",sid:"PFM152A"},
    ]
  },
  {
  title:"Elements of Screenwriting",
  cid:"PFM141",
  fee:35,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM141A : Wed 2:00-5:00 Heidekat",sid:"PFM141A"},
    {title:"PFM141B : Thur 6:00-9:00 Heidekat",sid:"PFM141B"}
    ]
  },
  {
  title:"Writing for Television",
  cid:"PFM243",
  fee:35,
  prereqs:"Prerequisite: Elements of Screenwriting. 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM243A : Thur 6:00-9:00 Calkins",sid:"PFM243A"},
	]
  },
  {
  title:"Developing the Feature Script",
  cid:"PFM241",
  fee:35,
  prereqs:"Prerequisite: Elements of Screenwriting (Introduction to Screenwriting). 42 classroom hours.",
  mediums:["fdv"],
  sections:[
    {title:"PFM241A : Tue 6:00-9:00 Monahan",sid:"PFM241A"},
	]
  },
  {
  title:"Elements of Film",
  cid:"PFM162",
  fee:35,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM162A : Tue 2:00-5:00 Howell",sid:"PFM162A"},
	]
  },
  {
  title:"Special Topics in Filmmaking: The Actor on Set",
  cid:"PFM271",
  fee:35,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","fdv"],
  sections:[
    {title:"PFM271A : Mon 6:00-9:00 Monahan",sid:"PFM271A"}
    ]
  },
  {
  title:"Photography 1",
  cid:"PFM121",
  fee:70,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","pht"],
  sections:[
    {title:"PFM121A : Mon 6:00-9:00 Hall",sid:"PFM121A"},
    {title:"PFM121B : Thur 6:00-9:00 Sambrick",sid:"PFM121B"},
    ]
  },
  {
  title:"Photography 2",
  cid:"PFM221",
  fee:70,
  prereqs:"Prerequisite: Photography 1 (Basic Digital Photography). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PFM221A : Wed 6:00-9:00 Sambrick",sid:"PFM221A"},
    ]
  },
  {
  title:"35mm Photography and Darkroom",
  cid:"PFM122",
  fee:70,
  prereqs:"Prerequisite: None. 42 classroom hours.",
  mediums:["noprereqs","pht"],
  sections:[
    {title:"PFM122A : Tue 6:00-9:00 Tohara",sid:"PFM122A"},
  {title:"PFM122B : Sat 10:30-1:30 Kelly",sid:"PFM122B"},
    ]
  },
  {
  title:"Medium Format Photography",
  cid:"PFM222",
  fee:70,
  prereqs:"Prerequisite: 35mm Photography and Darkroom (Black and White Photography 1). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PFM222A : Wed 6:00-9:00 Tohara",sid:"PFM222A"}
    ]
  },
  {
  title:"Portrait Photography",
  cid:"PFM230",
  fee:70,
  prereqs:"Prerequisite: Photography 1 OR 35mm Photography and Darkroom (Basic Digital Photography or Black and White Photography 1). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PFM230A : Thur 6:00-9:00 Kelly",sid:"PFM230A"}
    ]
  },
  {
  title:"Photo Editing 1",
  cid:"PFM225",
  fee:70,
  prereqs:"Prerequisite: Photography 1 (Basic Digital Photography). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PFM225A : Mon 6:00-9:00 Young",sid:"PFM225A"}
    ]
  },
  {
  title:"Special Topics in Photography: Imagining Futures in Photography",
  cid:"PFM272",
  fee:70,
  prereqs:"Prerequisite: Photography 1 (Basic Digital Photography). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PFM272A : Thur 6:00-9:00 Wormsley",sid:"PFM272A"},
    ]
  },
  {
  title:"Special Topics in Photography: Low-light Photography",
  cid:"PFM272",
  fee:70,
  prereqs:"Prerequisite: Photography 1 (Basic Digital Photography). 42 classroom hours.",
  mediums:["pht"],
  sections:[
    {title:"PHT272B : Tue 6:00-9:00 Bechtold",sid:"PFM272B"}
    ]
  },
  {
  title:"Independent Study in Filmmaking or Photography",
  cid:"PFM371",
  fee:70,
  prereqs:"Prerequisite: Permission of instructor. 42 classroom hours.",
  mediums:["fdv","pht"],
  sections:[
    {title:"PFM371A : By Appointment",sid:"PFM371A"}
    ]
  },
   {
  title:"Internships in Filmmaking or Photography",
  cid:"PFM372",
  fee:0,
  prereqs:"Prerequisite: Permission of Internship Coordinator. 42 classroom hours.",
  mediums:["fdv","pht"],
  sections:[
    {title:"PFM372A : By Appointment",sid:"PFM372A"}
    ]
  },
  ]
  /* /classes */
};
